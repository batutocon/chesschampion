//
//
//  Copyright Chess.com, LLC. All rights reserved.
//
//

(function(){"use strict";var locationProviderConfig=["$locationProvider",function($locationProvider){$locationProvider.html5Mode({
enabled:true,requireBase:false,rewriteLinks:false})}];angular.module("chesscom").config(locationProviderConfig)})();
//# sourceMappingURL=web_nonspa_config.js.map