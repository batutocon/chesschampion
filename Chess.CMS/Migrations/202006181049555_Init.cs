namespace Chess.CMS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        ApplicationUserID = c.String(),
                        birthday = c.String(),
                        address = c.Boolean(nullable: false),
                        phone = c.String(),
                        note = c.String(),
                        type = c.String(),
                        ChampionID = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.AspNetUsers", "Employee_ID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "Employee_ID");
            AddForeignKey("dbo.AspNetUsers", "Employee_ID", "dbo.Employees", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Employee_ID", "dbo.Employees");
            DropIndex("dbo.AspNetUsers", new[] { "Employee_ID" });
            DropColumn("dbo.AspNetUsers", "Employee_ID");
            DropTable("dbo.Employees");
        }
    }
}
