﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Chess.CMS.Startup))]
namespace Chess.CMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
