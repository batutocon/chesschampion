﻿using Chess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Chess.CMS.Areas.Admin.Models
{
    public class RankAfterRoundViewModel
    {
        public Player player { get; set; }
        public int elo { get; set; }
        public int score { get; set; }
        public int changeScore { get; set; }
    }
}