﻿using Chess.CMS.Repository;
using Chess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Chess.CMS.Areas.Admin.Controllers
{
    public class RoundController : Controller
    {
        // GET: Admin/Round
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();

        // GET: Admin/CapHoc
        public ActionResult Index()
        {
            var lstRound = _unitOfWork.GetRepositoryInstance<Round>().GetAllRecords().OrderBy(o => o.ID).ToList();
            return View(lstRound);
        }

        public async Task<JsonResult> AddRound(string name, string note)
        {
            try
            {
                Round item = new Round();
                item.name = name.Trim();
                item.note = note.Trim();
                _unitOfWork.GetRepositoryInstance<Round>().Add(item);
                _unitOfWork.SaveChanges();

                TempData["message"] = "Thêm  mới thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới", JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> EditRound(long id, string name, string note)
        {
            try
            {
                Round item = new Round();
                item = _unitOfWork.GetRepositoryInstance<Round>().GetFirstOrDefaultByParameter(o => o.ID == id);
                if (item != null)
                {
                    item.name = name.Trim();
                    item.note = note.Trim();
                    _unitOfWork.GetRepositoryInstance<Round>().Update(item);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    TempData["error"] = "bản ghi không tồn tại";
                    return Json("bản ghi không tồn tại", JsonRequestBehavior.AllowGet);
                }

                TempData["message"] = "Cập nhật thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi Cập nhật: " + ex.Message;
                return Json("Lỗi Cập nhật", JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Delete(long id)
        {
            try
            {
                Round item = new Round();
                item = _unitOfWork.GetRepositoryInstance<Round>().GetFirstOrDefaultByParameter(o => o.ID == id);
                if (item != null)
                {
                    _unitOfWork.GetRepositoryInstance<Round>().Remove(item);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    TempData["error"] = "bản ghi không tồn tại";
                    return Json("bản ghi không tồn tại", JsonRequestBehavior.AllowGet);
                }

                TempData["message"] = "Xóa thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa", JsonRequestBehavior.AllowGet);
            }
        }
    }
}