﻿using Chess.CMS.Areas.Admin.Models;
using Chess.CMS.Repository;
using Chess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Chess.CMS.Areas.Admin.Controllers
{
    public class RankAfterRoundController : Controller
    {
        // GET: Admin/RankAfterRound
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        public ActionResult Index()
        {
            return View();
        }
        public List<Round> GetListRound()
        {
            List<Round> lstCity = new List<Round>();
            lstCity = _unitOfWork.GetRepositoryInstance<Round>().GetAllRecords().ToList();
            return lstCity;
        }
        public JsonResult LoadListRound()
        {
            string strCode = "";
            List<Round> lstParent = GetListRound();
            foreach (var item in lstParent)
            {
                strCode += "<option value = \"" + item.ID + "\" id = \"" + item.ID + "\">" + item.name + "</option>";
            }
            return Json(strCode, JsonRequestBehavior.AllowGet);
        }
        public async Task<ActionResult> GetRankAfterRound(int roundID)
        {
            try
            {
                var lstRAR = new List<RankAfterRoundViewModel>();
                var rar = new List<RankAfterRound>();
                if (roundID > 0 && roundID < 12)
                {
                    rar = _unitOfWork.GetRepositoryInstance<RankAfterRound>().GetListByParameter(x => x.roundID == roundID).ToList();
                    foreach (RankAfterRound item in rar)
                    {
                        RankAfterRoundViewModel model = new RankAfterRoundViewModel();
                        model.elo = item.newElo;
                        model.score = item.newScore;
                        model.changeScore = item.oldElo - item.newScore;
                        Player player = _unitOfWork.GetRepositoryInstance<Player>().GetFirstOrDefaultByParameter(x => x.ID == item.playerID);
                        model.player = player;
                        lstRAR.Add(model);
                    }
                    //lstRAR = lstRAR.OrderBy(x => x.elo).ToList();
                    return PartialView("RAR", lstRAR);
                }
                else
                {
                    TempData["error"] = "Vòng đấu không tồn tại";
                    return null;
                }

            }
            catch (Exception ex)
            {
                string mss = ex.Message;
                return null;
            }
        }
    }
}