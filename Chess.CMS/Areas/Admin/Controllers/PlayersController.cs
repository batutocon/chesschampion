﻿using Chess.CMS.Repository;
using Chess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Chess.CMS.Areas.Admin.Controllers
{
    public class PlayersController : Controller
    {
        private GenericUnitOfWork _unitOfWork = new GenericUnitOfWork();
        // GET: Admin/Players
        public ActionResult Index()
        {
            var lstPlayer = _unitOfWork.GetRepositoryInstance<Player>().GetAllRecords().OrderBy(o => o.ID).ToList();
            return View(lstPlayer);
        }
        public ActionResult AddPlayer()
        {
            return View();
        }
        public ActionResult EditPlayer(long id)
        {
            Player module = new Player();
            module = _unitOfWork.GetRepositoryInstance<Player>().GetFirstOrDefaultByParameter(o => o.ID == id);
            if (module != null)
            {
                return View(module);
            }
            else
            {
                TempData["error"] = "bản ghi không tồn tại";
                return Json("bản ghi không tồn tại", JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> AddToPlayer(string name, string birthday, string country, string elo,string score, string note)
        {
            try
            {
                Player item = new Player();
                item.name = name;
                item.birthday = DateTime.Parse(birthday);
                item.country = country;
                item.elo = int.Parse(elo);
                item.score = int.Parse(score);
                item.note = note;
                _unitOfWork.GetRepositoryInstance<Player>().Add(item);
                _unitOfWork.SaveChanges();

                TempData["message"] = "Thêm  mới thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi thêm mới: " + ex.Message;
                return Json("Lỗi thêm mới", JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> EditToPlayer(long id, string name, string birthday, string country, string elo,string score, string note)
        {
            try
            {
                Player item = new Player();
                item = _unitOfWork.GetRepositoryInstance<Player>().GetFirstOrDefaultByParameter(o => o.ID == id);
                if (item != null)
                {
                    item.name = name;
                    item.birthday = DateTime.Parse(birthday);
                    item.country = country;
                    item.elo = int.Parse(elo);
                    item.score = int.Parse(score);
                    item.note = note;
                    _unitOfWork.GetRepositoryInstance<Player>().Update(item);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    TempData["error"] = "bản ghi không tồn tại";
                    return Json("bản ghi không tồn tại", JsonRequestBehavior.AllowGet);
                }

                TempData["message"] = "Cập nhật thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi Cập nhật: " + ex.Message;
                return Json("Lỗi Cập nhật", JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                Player item = new Player();
                item = _unitOfWork.GetRepositoryInstance<Player>().GetFirstOrDefaultByParameter(o => o.ID == id);
                if (item != null)
                {
                    _unitOfWork.GetRepositoryInstance<Player>().Remove(item);
                    _unitOfWork.SaveChanges();
                }
                else
                {
                    TempData["error"] = "bản ghi không tồn tại";
                    return Json("bản ghi không tồn tại", JsonRequestBehavior.AllowGet);
                }

                TempData["message"] = "Xóa thành công!";
                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa", JsonRequestBehavior.AllowGet);
            }
        }
    }
}